## Demo de un API REST que utiliza JWT


vamos alevantar el proyecto

```sh
 mvn -f app\pom.xml clean package -Dmaven.test.skip=true spring-boot:run  

```

Probamos los endpoints

### linux
```sh
	#Se lanza una petición de login
	curl -i -H "Content-Type: application/json" -X POST -d '{ "username": "admin", "password": "password"}' http://localhost:8888/login
	
	# Recuperamos los usuarios dados de alta
	curl -H "Authorization: Bearer xxx.yyy.zzz" http://localhost:8888/users/
	
	# Damos de alta un nuevo usuario
	curl -i -H 'Content-Type: application/json' -H 'Authorization: Bearer xxx.yyy.zzz' -X POST -d '{ "username": "daenerys", "password": "dracarys"}' http://localhost:8888/users/
```

### Windows

En terminal cmd cambia un poco la sintaxis, pero la idea es la misma


```cmd
#Se lanza una petición de login
curl -X POST http://localhost:8888/login -H "Content-Type: application/json" -d "{\"username\":\"admin\",\"password\":\"password\"}" -i
	
# Recuperamos los usuarios dados de alta
curl -H "Authorization: Bearer x.y.z" http://localhost:8888/users/
	
	# Damos de alta un nuevo usuario
curl -i -H 'Content-Type: application/json' -H 'Authorization: Bearer  x.y.z' -X POST -d '{ "username": "daenerys", "password": "dracarys"}' http://localhost:8888/users/
```