package com.autentia.demo.jwt;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import com.autentia.demo.jwt.usuario.Usuario;
import com.autentia.demo.jwt.usuario.UsuarioRepository;

@Component
public class UsuarioInitializer implements ApplicationRunner {

    private final UsuarioRepository usuarioRepository;

    @Autowired
    public UsuarioInitializer(UsuarioRepository usuarioRepository) {
        this.usuarioRepository = usuarioRepository;
    }

    @Override
    public void run(ApplicationArguments args) {
        Usuario usuario = new Usuario();
        usuario.setUsername("admin");
        usuario.setPassword("$2a$10$XURPShQNCsLjp1ESc2laoObo9QZDhxz73hJPaEv7/cBha4pk0AgP.");
        usuarioRepository.save(usuario);
    }
}

