# Use a base image with Java 11 installed
FROM openjdk:11-jre-slim

# Set the working directory inside the container
WORKDIR /app

# Copy the JAR file (nombre-de-tu-app.jar) built by Spring Boot into the container at /app
COPY target/demo-jwt-0.0.1-SNAPSHOT.jar /app/app.jar

# Expose the port on which your Spring Boot application runs (por ejemplo, si la aplicación utiliza el puerto 8080)
EXPOSE 8080

# Command to run your Spring Boot application when the container starts
CMD ["java", "-jar", "app.jar"]
